/* Returns a string with the specified char inserted between every character */
const solveExcerciseD1 = (originalString, char) => {
  /* initialize a blank string */
  let resultingString = "";

  /* append every char of the original string and if there a char follows append the char */
  for (let i = 0; i < originalString.length; i++)
    resultingString +=
      originalString[i] + (!!originalString[i + 1] ? char : "");

  /* return the final string */
  return resultingString;
};

/* Returns a string with the specified char inserted after every third character */
const solveExcerciseD2 = (originalString, char) => {
  /* initialize a blank string */
  let resultingString = "";

  /* append the original string split every 3 characters by char */
  for (let i = 0; i <= originalString.length - 1; i++) {
    resultingString += originalString[i];

    if (!!originalString[i + 1] && (i + 1) % 3 === 0) resultingString += char;
  }

  /* return the resulting string */
  return resultingString;
};

if (typeof module !== "undefined" && module.exports)
  module.exports = {
    solveExcerciseD1: solveExcerciseD1,
    solveExcerciseD2: solveExcerciseD2,
  };
