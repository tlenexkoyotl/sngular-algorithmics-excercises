/* Returns a string with its chars in the reversed order except for the last character */
const solveExcerciseC1 = (originalString) => {
  /* initialize a blank string */
  let reversedString = "";

  /* append the chars of the reversed string except for the original string's last character */
  for (let i = originalString.length - 2; i >= 0; i--)
    reversedString += originalString[i];

  /*  join the reversed string and the original string's last character */
  reversedString += originalString[originalString.length - 1];

  /* return the final string */
  return reversedString;
};

/* Returns the original string concatenated by the reversed string */
const solveExcerciseC2 = (originalString) => {
  /* create a copy of the original string */
  let resultingString = originalString;

  /* append the original string in reversed order to the copy of the original string */
  for (let i = originalString.length - 1; i >= 0; i--)
    resultingString += originalString[i];

  /* return the resulting string */
  return resultingString;
};

/* Returns the original string split by a newline every "n" characters. "n" is 3 by default */
const solveExcerciseC3 = (originalString, n = 3) => {
  /* initialize a blank string */
  let resultingString = "";

  /* append the original string split every "n" characters by a newline */
  for (let i = 0; i < originalString.length; i++) {
    resultingString += originalString[i];

    if (!!originalString[i + 1] && (i + 1) % n === 0) resultingString += "\n";
  }
  /* return the resulting string */
  return resultingString;
};

if (typeof module !== "undefined" && module.exports)
  module.exports = {
    solveExcerciseC1: solveExcerciseC1,
    solveExcerciseC2: solveExcerciseC2,
    solveExcerciseC3: solveExcerciseC3,
  };
