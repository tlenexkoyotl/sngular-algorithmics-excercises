/* Returns a string with the initial character of every word. Words must be separated by spaces */
const solveExcerciseE1 = (originalString) => {
  /* initialize a blank string */
  let resultingString = "";
  /* first character is set as initial */
  let isInitial = true;

  /* append every initial character of the original string */
  for (let i = 0; i < originalString.length; i++) {
    /* figure whether the loop is currently at the beginning of the string or after a space. This means the char at postion "i" is an initial */
    isInitial = i === 0 || originalString[i - 1] === " ";

    /* if the char at postion "i" is an initial then append it to the string, otherwise append a blank char */
    resultingString += isInitial ? originalString[i] : "";
  }

  /* return the final string */
  return resultingString;
};

/* Returns a string with only the words beginning with the character "A" */
const solveExcerciseE2 = (originalString) => {
  /* initialize a blank string */
  let resultingString = "";
  /* initialize boolean for whether the current character is an initial as true from the beginning */
  let isInitial = true;
  /* initialize boolean for whether the current character is an initial and an "A" as true from the beginning*/
  let isInitialAnA = true;
  /* initialize originalString index as 0 */
  let i = 0;

  /* loop over originalString while it has characters */
  while (i < originalString.length) {
    /* check whether current character is an initial*/
    isInitial = i === 0 || originalString[i - 1] === " ";

    /* if current character is an initial then check whether it is an "A" */
    if (isInitial) isInitialAnA = ["a", "A"].includes(originalString[i]);

    /* if current character is an initial and an "A" */
    if (isInitialAnA) {
      /* if the resulting string is empty and the previous character is a space */
      if (resultingString.length > 0 && originalString[i - 1] === " ")
        /* append a space to the resulting string */
        resultingString += " ";

      /* if the current character is not a space append it to the resulting string  */
      if (originalString[i] !== " ") resultingString += originalString[i];
    }

    /* increase index */
    i++;
  }

  /* return the final string */
  return resultingString;
};

/* Returns a boolean indicating whether the input string is a palindrome (it is read the same when reversed, not considering punctuation, spaces nor capitalization) */
const solveExcerciseE3 = (inputString) => {
  inputString = inputString.toLowerCase();
  /* initialize an empty string */
  let originalString = "";
  /* initialize an empty string */
  let reversedString = "";
  /* initialize punctuation array */
  let punctuation = [
    " ",
    '"',
    "'",
    ",",
    ".",
    ";",
    ":",
    "[",
    "]",
    "(",
    ")",
    "¡",
    "!",
    "¿",
    "?",
    "@",
    "#",
    "$",
    "%",
    "^",
    "&",
    "*",
    "-",
    "=",
    "+",
    "/",
    "<",
    ">",
  ];

  /* loop over the original string */
  for (let i = 0; i < inputString.length; i++)
    /* if current character is NOT a punctuation sign or space append it to the originalString */
    if (!punctuation.includes(inputString[i])) originalString += inputString[i];

  /* loop over the original string in reverse order */
  for (let i = originalString.length - 1; i >= 0; i--)
    reversedString += originalString[i];

  /* return whether the reversed string is strictly equal to the input string */
  return originalString === reversedString;
};

if (typeof module !== "undefined" && module.exports)
  module.exports = {
    solveExcerciseE1: solveExcerciseE1,
    solveExcerciseE2: solveExcerciseE2,
    solveExcerciseE3: solveExcerciseE3,
  };
