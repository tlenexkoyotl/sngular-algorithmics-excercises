const emptyResultingString = (values, string1, string2) => {
  /* if the resulting substring was not empty set it back to empty and reset string1's index */
  if (
    0 < values.resultingString.length &&
    values.resultingString.length < string1.length
  ) {
    values.i = 0;
    values.resultingString = "";
  }

  /* if within the remaining chars of string 2 a substring of string 1 could no longer fit, terminate looping */
  if (string2.length - values.j < string1.length) {
    values.i = string1.length;
    values.j = string2.length;
  }

  /* increase string2's index */
  values.j++;
};

const appendResultingString = (values, string1, string2) => {
  /* if the char in string1's current position equals that in string2's current position */
  if (string1[values.i] === string2[values.j]) {
    if (values.resultingString.length === string1.length) {
      values.i = string1.length;
      values.j = string2.length;
    }

    values.resultingString += string1[values.i++];
    values.j++;
  } /* when the chars compared do not equal one another */ else {
    emptyResultingString(values, string1, string2);
  }
};

const loopIfIndexesFitArray = (values, string1, string2) => {
  if (values.resultingString.length < string1.length) {
    /* while looping over the strings make sure the index looping over string1 does not overcome its length and the same goes for string2, otherwise terminate both loops */
    if (values.i < string1.length && values.j < string2.length) {
      appendResultingString(values, string1, string2);
    } else {
      /* terminate both loops */
      values.i = string1.length;
      values.j = string2.length;

      if (values.resultingString.length < string1.length)
        values.resultingString = "";
    }
  } else {
    values.i = string1.length;
    values.j = string2.length;
  }
};

const loopOverStrings = (resultingString, string1, string2) => {
  const values = { i: 0, j: 0, resultingString: resultingString };

  /* loop over the longer string */
  for (; values.j < string2.length; ) {
    /* while looping over the longer string also loop over the shorter string */
    for (; values.i < string1.length; )
      loopIfIndexesFitArray(values, string1, string2);

    break;
  }

  return values.resultingString;
};

const isString1LongerThan2 = (string1, string2) => {
  /* initialize resultingString as blank */
  let resultingString = "";

  /* if string1 is the same length as or longer than string2 then assume string1 could be substring of string2, otherwise call itself again using parameters in reversed order */
  if (string1.length <= string2.length) {
    resultingString = loopOverStrings(resultingString, string1, string2);
  } /* return the result of itself using parameters in reversed order */ else
    return solveExcerciseF1(string2, string1);

  /* return whether the resulting substring is not empty */
  return resultingString === string1;
};

/* Returns a boolean indicating whether a string is another's substring */
const solveExcerciseF1 = (string1, string2) => {
  /* if strings equal one another they are the subtring of the other */
  if (string1 === string2) return true;

  return isString1LongerThan2(string1, string2);
};

const arrayFromWords = (originalString) => {
  let word = "";
  let words = [];

  for (let i = 0; i <= originalString.length; i++) {
    if (originalString[i] !== " " && !!originalString[i])
      word += originalString[i];
    else {
      words = [...words, word];
      word = "";
    }
  }

  return words;
};

const compareTwoWords = (word1, word2) => {
  word1 = word1.toLowerCase();
  word2 = word2.toLowerCase();

  if (word1 < word2) return -1;
  if (word1 > word2) return 1;

  return 0;
};

const sortWords = (words) => {
  let temp;

  for (let i = 0; i < words.length; i++)
    for (let j = i + 1; j < words.length; j++)
      if (compareTwoWords(words[i], words[j]) > 0) {
        temp = words[i];
        words[i] = words[j];
        words[j] = temp;
      }

  return words;
};

const joinWords = (words) => {
  let joined = "";

  for (let i = 0; i < words.length; i++) {
    joined += `${words[i]}`;

    if (!!words[i + 1]) joined += " ";
  }
  return joined;
};

/* Returns a string with the words in alphabetical order */
const solveExcerciseF2 = (originalString) => {
  let words = arrayFromWords(originalString);
  words = sortWords(words);

  return joinWords(words);
};

if (typeof module !== 'undefined' && module.exports)
  module.exports = {
    solveExcerciseF1: solveExcerciseF1,
    solveExcerciseF2: solveExcerciseF2,
  };
