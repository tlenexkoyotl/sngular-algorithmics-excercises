const assert = require("assert");

const excerciseC = require("../src/excerciseC");

suite("Excercise C", function () {
  test("should return the input string reversed except for the last character which must remain at the end", function () {
    const testDataSets = [
      {
        expected: "uhuh1",
        input: "huhu1",
      },
      {
        expected: "odnuM aloH!",
        input: "Hola Mundo!",
      },
    ];

    for (const { expected, input } of testDataSets) {
      const result = excerciseC.solveExcerciseC1(input);

      assert.strictEqual(expected, result);
    }
  });

  test("should return the input string concatenated by the reversed string", function () {
    const testDataSets = [
      {
        expected: "reflejoojelfer",
        input: "reflejo",
      },
      {
        expected: "huhu11uhuh",
        input: "huhu1",
      },
      {
        expected: "Hola Mundo!!odnuM aloH",
        input: "Hola Mundo!",
      },
    ];

    for (const { expected, input } of testDataSets) {
      const result = excerciseC.solveExcerciseC2(input);

      assert.strictEqual(expected, result);
    }
  });

  test('should return the input string split by a newline every "n" characters', function () {
    const testDataSets = [
      {
        expected: "Hol\na M\nund\no!",
        input: {
          originalString: "Hola Mundo!",
          n: 3,
        },
      },
      {
        expected: "r\ne\nf\nl\ne\nj\no",
        input: {
          originalString: "reflejo",
          n: 1,
        },
      },
      {
        expected: "Lorem\n ipsu\nm dol\nor si\nt ame\nt",
        input: {
          originalString: "Lorem ipsum dolor sit amet",
          n: 5,
        },
      },
    ];

    for (const {
      expected,
      input: { originalString, n },
    } of testDataSets) {
      const result = excerciseC.solveExcerciseC3(originalString, n);

      assert.strictEqual(expected, result);
    }
  });
});
