const assert = require("assert");

const excerciseD = require("../src/excerciseD");

suite("Excercise D", function () {
  test("should return the input string with the specified char inserted between every character", function () {
    const testDataSets = [
      {
        expected: "s,e,p,a,r,a,r",
        input: {
          originalString: "separar",
          char: ",",
        },
      },
      {
        expected: "s_n_a_k_e_c_a_s_e",
        input: {
          originalString: "snakecase",
          char: "_",
        },
      },
      {
        expected: "r\ne\nf\nl\ne\nj\no",
        input: {
          originalString: "reflejo",
          char: "\n",
        },
      },
    ];

    for (const {
      expected,
      input: { originalString, char },
    } of testDataSets) {
      const result = excerciseD.solveExcerciseD1(originalString, char);

      assert.strictEqual(expected, result);
    }
  });

  test("should return the input string with the specified char inserted after every third character", function () {
    const testDataSets = [
      {
        expected: "255.255.255.0",
        input: {
          originalString: "2552552550",
          char: ".",
        },
      },
      {
        expected: "sep,ara,r",
        input: {
          originalString: "separar",
          char: ",",
        },
      },
      {
        expected: "sna_kec_ase",
        input: {
          originalString: "snakecase",
          char: "_",
        },
      },
      {
        expected: "ref\nlej\no",
        input: {
          originalString: "reflejo",
          char: "\n",
        },
      },
    ];

    for (const {
      expected,
      input: { originalString, char },
    } of testDataSets) {
      const result = excerciseD.solveExcerciseD2(originalString, char);

      assert.strictEqual(expected, result);
    }
  });
});
