const assert = require("assert");

const excerciseE = require("../src/excerciseE");

suite("Excercise E", function () {
  test("should return the input string with the initial character of every word", function () {
    const testDataSets = [
      {
        expected: "USB",
        input: "Universal Serial Bus",
      },
      {
        expected: "POTUS",
        input: "President Of The United States",
      },
      {
        expected: "FIB",
        input: "Federal Investigation Bureau",
      },
      {
        expected: "NSA",
        input: "National Security Agency",
      },
      {
        expected: "CIA",
        input: "Central Intelligence Agency",
      },
      {
        expected: "CTB",
        input: "Central Tamale Bureau",
      },
    ];

    for (const { expected, input } of testDataSets) {
      const result = excerciseE.solveExcerciseE1(input);

      assert.strictEqual(expected, result);
    }
  });

  test("should return the input string with the specified char inserted after every third character", function () {
    const testDataSets = [
      {
        expected: "Antes ayer",
        input: "Antes de ayer",
      },
      {
        expected: "Ahora a",
        input: "Ahora vamos a comer",
      },
      {
        expected: "",
        input: "Ya es hora de triunfar",
      },
      {
        expected: "anita arriba",
        input: "anita lava la tina arriba",
      },
      {
        expected: "a asador allá arriba ahorita",
        input: "voy a traer el asador de allá arriba y ahorita vengo",
      },
    ];

    for (const { expected, input } of testDataSets) {
      const result = excerciseE.solveExcerciseE2(input);

      assert.strictEqual(expected, result);
    }
  });

  test("should return a boolean indicating whether the input string is a palindrome", function () {
    const testDataSets = [
      {
        expected: false,
        input: "Antes de ayer",
      },
      {
        expected: false,
        input: "Ahora vamos a comer",
      },
      {
        expected: false,
        input: "Ya es hora de triunfar",
      },
      {
        expected: false,
        input: "anita lava la tina arriba",
      },
      {
        expected: true,
        input: "anita lava la tina",
      },
      {
        expected: true,
        input: "Anita lava la tina",
      },
      {
        expected: false,
        input: "voy a traer el asador de allá arriba y ahorita vengo",
      },
      {
        expected: true,
        input: "sator arepo tenet opera rotas",
      },
      {
        expected: true,
        input: "Sator arepo tenet opera rotas",
      },
      {
        expected: true,
        input: "was it a car or a cat I saw",
      },
      {
        expected: true,
        input: "Was it a car or a cat I saw",
      },
      {
        expected: true,
        input: "Was it a car or a cat I saw?",
      },
      {
        expected: true,
        input: "murder for a jar of red rum",
      },
      {
        expected: true,
        input: "Murder for a jar of red rum",
      },
      {
        expected: true,
        input: "go hang a salami Im a lasagna hog",
      },
      {
        expected: true,
        input: "Go hang a salami Im a lasagna hog",
      },
      {
        expected: true,
        input: "go hang a salami im a lasagna hog",
      },
      {
        expected: true,
        input: "Go hang a salami im a lasagna hog",
      },
      {
        expected: true,
        input: "Go hang a salami, I'm a lasagna hog",
      },
      {
        expected: true,
        input: "Mr Owl ate my metal worm",
      },
      {
        expected: true,
        input: "Mr owl ate my metal worm",
      },
      {
        expected: true,
        input: "mr Owl ate my metal worm",
      },
      {
        expected: true,
        input: "mr owl ate my metal worm",
      },
      {
        expected: true,
        input: "Mr. Owl ate my metal worm",
      },
      {
        expected: true,
        input: "Do geese see God",
      },
      {
        expected: true,
        input: "Do geese see god",
      },
      {
        expected: true,
        input: "do geese see God",
      },
      {
        expected: true,
        input: "do geese see god",
      },
      {
        expected: true,
        input: "Do geese see God?",
      },
      {
        expected: true,
        input: "Rats live on no evil star",
      },
      {
        expected: true,
        input: "Live on time, emit no evil",
      },
      {
        expected: true,
        input: "Step on no pets",
      },
    ];

    for (const { expected, input } of testDataSets) {
      const result = excerciseE.solveExcerciseE3(input);

      assert.strictEqual(expected, result);
    }
  });
});
