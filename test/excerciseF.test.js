const assert = require("assert");

const excerciseF = require("../src/excerciseF");

suite("Excercise F", function () {
  test("should return a boolean indicating whether a string is another's substring", function () {
    let testDataSets = [
      {
        expected: false,
        input: {
          string1: "cadenas",
          string2: "subcadena",
        },
      },
      {
        expected: false,
        input: {
          string1: "subcadena",
          string2: "cadenas",
        },
      },
      {
        expected: false,
        input: {
          string1: "cadens",
          string2: "subcadena",
        },
      },
      {
        expected: false,
        input: {
          string1: "subcadena",
          string2: "cadens",
        },
      },
      {
        expected: false,
        input: {
          string1: "caddna",
          string2: "subcadena",
        },
      },
      {
        expected: false,
        input: {
          string1: "subcadena",
          string2: "caddna",
        },
      },
      {
        expected: true,
        input: {
          string1: "cadena",
          string2: "subcadena",
        },
      },
      {
        expected: true,
        input: {
          string1: "subcadena",
          string2: "cadena",
        },
      },
      {
        expected: true,
        input: {
          string1: "Antes de ayer",
          string2: "Antes de ayer",
        },
      },
      {
        expected: true,
        input: {
          string1: "Antes de ayer",
          string2: "Antes de ayer vine",
        },
      },
      {
        expected: true,
        input: {
          string1: "Antes de ayer",
          string2: "Yo Antes de ayer vine",
        },
      },
      {
        expected: false,
        input: {
          string1: "I'm solving a few algorithmics excercises",
          string2: "algorithmic excercises",
        },
      },
      {
        expected: true,
        input: {
          string1: "I'm solving a few algorithmics excercises",
          string2: "algorithmics excercises",
        },
      },
      {
        expected: true,
        input: {
          string1:
            "I'm solving a few algorithmics excercises to excel in an interview",
          string2: "algorithmics excercises",
        },
      },
      {
        expected: true,
        input: {
          string1: "Antes de ayer",
          string2: "Antes de ayer",
        },
      },
      {
        expected: true,
        input: {
          string1: "Antes de ayer vine",
          string2: "Antes de ayer",
        },
      },
      {
        expected: true,
        input: {
          string1: "Yo Antes de ayer vine",
          string2: "Antes de ayer",
        },
      },
      {
        expected: false,
        input: {
          string1: "algorithmic excercises",
          string2: "I'm solving a few algorithmics excercises",
        },
      },
      {
        expected: true,
        input: {
          string2: "I'm solving a few algorithmics excercises",
          string1: "algorithmics excercises",
        },
      },
      {
        expected: true,
        input: {
          string1: "algorithmics excercises",
          string2:
            "I'm solving a few algorithmics excercises to excel in an interview",
        },
      },
      {
        expected: true,
        input: {
          string1: "a",
          string2:
            "I'm solving a few algorithmics excercises to excel in an interview",
        },
      },
      {
        expected: true,
        input: {
          string1:
            "I'm solving a few algorithmics excercises to excel in an interview",
          string2: "a",
        },
      },
    ];

    for (const {
      expected,
      input: { string1, string2 },
    } of testDataSets) {
      const result = excerciseF.solveExcerciseF1(string1, string2);

      assert.strictEqual(expected, result);
    }
  });

  test("should return a a string with the words in alphabetical order", function () {
    let testDataSets = [
      {
        expected: "Alberto Juan",
        input: "Juan Alberto",
      },
      {
        expected: "aLbErTo jUaN",
        input: "jUaN aLbErTo",
      },
      {
        expected: "Pablo Pedro",
        input: "Pedro Pablo",
      },
      {
        expected: "pAbLo pEdRo",
        input: "pEdRo pAbLo",
      },
      {
        expected: "A B F G X Y Z",
        input: "Z G X Y A B F",
      },
      {
        expected: "a B f G x Y z",
        input: "z G x Y a B f",
      },
      {
        expected: "Alfa Alfe Beta Beto Fafa Fufu Gamma Xi Ypsilon Zeta",
        input: "Zeta Fafa Gamma Alfe Beto Xi Ypsilon Alfa Beta Fufu",
      },
      {
        expected: "Antes ayer de",
        input: "Antes de ayer",
      },
      {
        expected: "Antes ayer de vine",
        input: "Antes de ayer vine",
      },
      {
        expected: "Antes ayer de vine Yo",
        input: "Yo Antes de ayer vine",
      },
      {
        expected:
          "a algorithmics an excel excercises few I'm in interview solving to",
        input: "I'm solving a few algorithmics excercises to excel in an interview",
      },
    ];

    for (const { expected, input } of testDataSets) {
      const result = excerciseF.solveExcerciseF2(input);

      assert.strictEqual(expected, result);
    }
  });
});
